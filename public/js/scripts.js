jQuery(function($){

    'use strict';



    // ----------------------------------------------
    // Preloader
    // ----------------------------------------------
    (function () {
        $(window).load(function() {
            $('#pre-status').fadeOut();
            $('#st-preloader').delay(350).fadeOut('slow');
        });
    }());



    // ----------------------------------------------
    //  magnific-popup
    // ----------------------------------------------
    (function () {

        $('.portfolio-items').magnificPopup({
            delegate: 'a',
            type: 'image',
            // other options
            closeOnContentClick: false,
            closeBtnInside: false,
            mainClass: 'mfp-with-zoom mfp-img-mobile',

            gallery: {
                enabled: false
            },
            zoom: {
                enabled: true,
                duration: 300, // don't foget to change the duration also in CSS
                opener: function(element) {
                    return element.find('i');
                }
            }

        });

    }());



    // ----------------------------------------------
    // Fun facts
    // ----------------------------------------------
    (function () {
        $('.st-counter').counterUp({
            delay: 10,
            time: 1500
        });
    }());



    // ----------------------------------------------
    //  Isotope Filter
    // ----------------------------------------------
    (function () {
        var winDow = $(window);
        var $container=$('.portfolio-items');
        var $filter=$('.filter');

        try{
            $container.imagesLoaded( function(){
                $container.show();
                $container.isotope({
                    filter:'*',
                    layoutMode:'masonry',
                    animationOptions:{
                        duration:750,
                        easing:'linear'
                    }
                });
            });
        } catch(err) {
        }

        winDow.bind('resize', function(){
            var selector = $filter.find('a.active').attr('data-filter');

            try {
                $container.isotope({
                    filter	: selector,
                    animationOptions: {
                        duration: 750,
                        easing	: 'linear',
                        queue	: false,
                    }
                });
            } catch(err) {
            }
            return false;
        });

        $filter.find('a').click(function(){
            var selector = $(this).attr('data-filter');

            try {
                $container.isotope({
                    filter	: selector,
                    animationOptions: {
                        duration: 750,
                        easing	: 'linear',
                        queue	: false,
                    }
                });
            } catch(err) {

            }
            return false;
        });


        var filterItemA	= $('.filter a');

        filterItemA.on('click', function(){
            var $this = $(this);
            if ( !$this.hasClass('active')) {
                filterItemA.removeClass('active');
                $this.addClass('active');
            }
        });
    }());


    // -------------------------------------------------------------
    // masonry
    // -------------------------------------------------------------

    (function () {
        var $container = $('.portfolio-items');
        // initialize
        $container.masonry({
            itemSelector: '.work-grid'
        });
    }());


    // -------------------------------------------------------------
    // Animated scrolling / Scroll Up
    // -------------------------------------------------------------

    (function () {
        $('li a[href*=#]').bind("click", function(e){
            var anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $(anchor.attr('href')).offset().top -79
            }, 1000);
            e.preventDefault();
        });
    }());


    // ----------------------------------------------
    // Owl Carousel
    // ----------------------------------------------
    (function () {

        $(".testimonials-part").owlCarousel({
            singleItem:true,
            lazyLoad : true,
            pagination:false,
            navigation : false,
            autoPlay: true,
        });

    }());


    // -------------------------------------------------------------
    // Back To Top
    // -------------------------------------------------------------

    (function () {
        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('.scroll-up').fadeIn();
            } else {
                $('.scroll-up').fadeOut();
            }
        });
    }());


    // ----------------------------------------------
    // Parallax Scrolling
    // ----------------------------------------------
    (function () {
        $(window).bind('load', function () {
            parallaxInit();
        });
        function parallaxInit() {
            $("#testimonials-part").parallax("50%", 0.3);
        }
        parallaxInit();
    }());


    // ----------------------------------------------
    // fitvids js
    // ----------------------------------------------
    (function () {
        $(".post-video").fitVids();
    }());


    // ----------------------------------------------
    // Navigation Bar ( Navbar )
    // ----------------------------------------------
    $(window).load(function() {
        // Navbar Init
        $('nav').addClass('original').clone().insertAfter('nav').addClass('navbar-fixed-top').css('position', 'fixed').css('top', '0').css('margin-top', '0').removeClass('original');
        $('.mobile-nav ul').html($('nav .navbar-nav').html());
        $('nav.navbar-fixed-top .navbar-brand img').attr('src', $('nav.navbar-fixed-top .navbar-brand img').data("active-url"));
    });

    // Window Scroll
    function onScroll() {
        if ($(window).scrollTop() > 50) {
            $('nav.original').css('opacity', '0');
            $('nav.navbar-fixed-top').css('opacity', '1');
        } else {
            $('nav.original').css('opacity', '1');
            $('nav.navbar-fixed-top').css('opacity', '0');
        }
    }
    window.addEventListener('scroll', onScroll, false);

    // Mobile Nav
    $('body').on('click', 'nav .navbar-toggle', function() {
        event.stopPropagation();
        $('.mobile-nav').addClass('active');
    });

    $('body').on('click', '.mobile-nav a', function(event) {
        $('.mobile-nav').removeClass('active');
        if(!this.hash) return;
        event.preventDefault();
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            event.stopPropagation();
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    $('body').on('click', '.mobile-nav a.close-link', function(event) {
        $('.mobile-nav').removeClass('active');
        event.preventDefault();
    });

    $('body').on('click', 'nav.original .navbar-nav a:not([data-toggle])', function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            event.stopPropagation();
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

});