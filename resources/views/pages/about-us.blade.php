        <section class="about-j-tech" id="about-j-tech">
            <div class="content">
                <div class="container-template">
                    <div class="headergroup">
                        <h2 class="in-point">
                            <span class="seconday">What Is</span>
                            <span>J-tech Co.?</span>
                        </h2>
                    </div>
                    <div class="intro content-template">
                        <p>
                            We build powerful and responsive websites that respond to the users devices, platforms and screen sizes
                            so that you can see the same desktop version layout in Mobile,Tablet & iPad
                        </p>

                        <p>
                        <a href="{{url('test-responsive-website')}}" class="btn btn-about">
                            See what we mean
                        </a>
                            <!-- <span class="text-slant">
                                <i class="fa fa-quote-left" aria-hidden="true"></i>
                                    Well' since most consumers are using an ever increasing
                                    variety	of platforms to access services. Designing a website only for one
                                    type of device will leave some of your users with a frustrating
                                    experience. Therefore ...
                                <i class="fa fa-quote-right" aria-hidden="true"></i>
                            </span> -->
                        </p>
                    </div>

                    <ul class="timeline">
                        <li class="year"><i class="fa fa-flag" aria-hidden="true"></i></li>
                        <li class="upper-line">
                            <span class="title">Our Goal</span>
                                <span class="timeline-content">
                                    To make our clients advertising more successful by
                                    utilizing our strategic insight and the latest
                                    communication tools to provide targeted and measurable
                                    marketing campaign.
                                </span>
                        </li>
                        <li class="year"><i class="fa fa-star" aria-hidden="true"></i></li>
                        <li class="upper-line">
                            <span class="title">Why Choose Us ?</span>
                                <span class="timeline-content">
                                    We seek to differentiate your brands and deliver a
                                    strong return on your marketing investment while
                                    being a valued and seamless extension of your
                                    marketing efforts.
                                </span>
                        </li>
                    </ul>

                </div>
            </div>
        </section>
        <!-- --------------------------   -------------- ------------------------ -->
        <section id="test" class="test">
            <div class="content">
                <p class="text-center">
                    Would you like to test your website just to see if its mobile friendly?
                </p>
                <p class="text-center">
                    <a onclick="{!! Analytics::trackEvent('Test Visitors Website', 'pageview') !!}" href="{{url ('test-responsive-website')}}" class="btn btn-about" style="margin-bottom: 30px;">
                        Click here
                    </a>
                </p>
                <div class="social-icons text-center">
                <p>Share this on</p>
                    <i class="fa fa-facebook box" aria-hidden="true"></i>
                    <i class="fa fa-twitter box" aria-hidden="true"></i>
                    <i class="fa fa-whatsapp box" aria-hidden="true"></i>
                    <i class="fa fa-google-plus box" aria-hidden="true"></i>
                </div>
            </div>

        </section>

