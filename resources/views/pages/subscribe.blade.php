<section id="subscribe">
    <div class="container">
        <div class="row">
            <div class="subscribe-overlay"></div>
            <div class="col-md-8 col-md-offset-2 col-sm-12">
                <div class="st-subscribe">

                    <div class="item active text-center">
                        <h2>CONNECT WITH US</h2>
                        <p>"Subscribe for marketing tips and updates"</p>
                        <div class="st-border"></div>
                        <div class="client-info">
                            <form action="{{'send-email'}}" class="subscribe-form" name="subscribe-form" method="post">
                                <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                <div class="input-group">
                                    <input type="email" name="subscription_email" placeholder="Enter your email*" class="form-control" aria-label="glyphicon glyphicon-flag">
                                    <div class="input-group-btn">
                                        <input type="submit" name="submit" value="Let's Go" class="btn subscribe-btn-send">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>