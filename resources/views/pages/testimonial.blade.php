        <section class="testimonials-part" id="testimonials-part">
            <div class="testimonials-part-section-seperator">
                <div class="container-template">
                    <h2>
                        <span class="seconday">What our</span>Clients say
                    </h2>

                    <div class="testimonials-container">
                        <ul class="testimonials testimonials-wrapper">
                            <li class="testimonials-slide">
                                <span class="testimonial-text">
                                    We wanted a site that conveyed the progressive vision of
                                    Organisation, and the J-tech team delivered!
                                </span>
                                <span class="testimonial-author">
                                    Andrew Budembeshi, Cyber Crime Investigator
                                </span>
                            </li>
                        </ul>
                    </div>

                    <div class="testimonials-pagination"></div>
                </div>
            </div>
        </section>
