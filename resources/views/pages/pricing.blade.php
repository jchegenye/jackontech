        <section class="pricing-part" id="pricing-part">
            <div class="section-seperator">
                <div class="container-template">
                    <h2>
                        <span class="seconday">Our</span> Pricing
                    </h2>
                </div>
            </div>
            <div class="content">
                <div class="container-template">
                    <div class="headergroup">
                        <h2>
                            <i class="icon-cogs size-25"></i>
                        </h2>
                    </div>

                    <div class="intro content-template">
                        <p>
                            There is no conventional standard for web design charges.
                            Charges vary depending on your specific requirements & features.
                        </p>
                        <br>
                        <p>
                            We do not intend to scare you away with our pricing levels but
                            instead they are meant to give you a glimpse into what you are
                            likely to spend for your project.
                        </p>
                    </div>
                    <br>

                    <a class="quote-btn" data-toggle="collapse" data-parent="#collapseQuote" href="#collapseQuote" class="smoothScroll">Get A Quote</a>
                    <div class="panel">
                        @if ($errors->has('full_names','your_email','project_type','project_description','county','hear_about_us'))
                            <div id="collapseQuote" class="panel-collapse collapse in">
                                @elseif(Session::has('internet_connection'))
                                    <div id="collapseQuote" class="panel-collapse collapse in"></div>
                                @elseif(Session::has('successful_quote'))
                                    <div id="collapseQuote" class="panel-collapse collapse in"></div>
                                @else
                                    <div id="collapseQuote" class="panel-collapse collapse">
                        @endif
                            <div class="panel-body">

                                <form id="contact-us" method="post" action="{{url('save-quote')}}">
                                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                    <div class="col-xs-6 ">
                                        <div class="form-group">
                                            <input type="text" name="full_names" placeholder="Full Names*" class="form" value="{{Input::old('full_names')}}">
                                            @if ($errors->has('full_names'))
                                                <span class="text text-danger">{{ $errors->first('full_names') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="company_name" placeholder="Company Name" class="form " value="{{Input::old('company_name')}}">
                                            @if ($errors->has('company_name'))
                                                <span class="text text-danger">{{ $errors->first('company_name') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="phone_number" placeholder="Phone number" class="form " value="{{Input::old('phone_number')}}">
                                            @if ($errors->has('phone_number'))
                                                <span class="text text-danger">{{ $errors->first('phone_number') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="your_email" placeholder="Email*" class="form " value="{{Input::old('your_email')}}">
                                            @if ($errors->has('your_email'))
                                                <span class="text text-danger">{{ $errors->first('your_email') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="project_type" placeholder="Project Type / Service's*" class="form " value="{{Input::old('project_type')}}">
                                            @if ($errors->has('project_type'))
                                                <span class="text text-danger">{{ $errors->first('project_type') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <textarea rows="40" name="project_description" id="bodyMessage" class="form textarea" placeholder="Give us some details about your project, time-line and budget.*" value="{{Input::old('project_description')}}"></textarea>
                                            @if ($errors->has('project_description'))
                                                <span class="text text-danger">{{ $errors->first('project_description') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="county" placeholder="From which county?" class="form " value="{{Input::old('county')}}">
                                            @if ($errors->has('county'))
                                                <span class="text text-danger">{{ $errors->first('county') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <select name="hear_about_us" class="form">
                                                <option selected class="form-control" value="{{Input::old('hear_about_us')}}">
                                                    Where did you hear About Us?<span>*</span>
                                                </option>
                                                <option>Facebook</option>
                                                <option>Twitter</option>
                                                <option>WhatsApp</option>
                                                <option>Gmail</option>
                                                <option>A friend</option>
                                                <option>Our Website</option>
                                                <option>Other</option>
                                            </select>
                                            @if ($errors->has('hear_about_us'))
                                                <span class="text text-danger">{{ $errors->first('hear_about_us') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="relative fullwidth col-xs-12">
                                        <button type="submit" id="submit" name="submit" class="form-btn semibold">Send</button>
                                    </div>
                                    <div class="clear"></div>
                                </form>

                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="social-icons text-center">
                    <p>Share this on</p>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-whatsapp"></i></a>
                </div>
            </div>
        </section>
