        <section class="get-services" id="get-services">
            <div class="get-services-section-seperator" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="0">
                <div class="container-template">
                    <i class="icon-shopping-cart size-50"></i>
                    <div class="headergroup">
                        <h2>
                            <span class="seconday">Get</span>
                            <span>Get Our Services</span>
                        </h2>
                    </div>

                    <div class="content-template">
                        <p>
                            We believe by now you have a clear picture of what we do, who we are
                            and the service we offer to our clients.
                        </p>
                        <p>
                            Do you like some of our works or is your website not mobile friendly and your are interested in doing
                            business with us?
                        </p>
                        <p>
                            Why not take a step and talk to us today.
                        </p>
                    </div>
                    <br>
                    <a onclick="{!! Analytics::trackEvent('Get Our Services', 'pageview') !!}"
                       href="#contact-form" class="smoothScroll form-btn semibold">Go A Head</a>
                </div>
            </div>
        </section>
