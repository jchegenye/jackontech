        <section class="section-intro" id="section-intro" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="0">
            <div class="intro-content"><div class="intro-content-cover"></div>
                <h1>
                    {{--<img src="logo.png" class="img-responsive">--}}
                    <span class="seconday">
                        Your number one responsive website development company in the Kenyan market, today.
                    </span>
                    <span></span>
                </h1>
            </div>
        </section>
