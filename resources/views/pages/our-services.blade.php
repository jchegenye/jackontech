        <section class="stastistical-part" id="stastistical-part">
            <div class="section-seperator">
                <div class="container-template">
                    <h2>
                        <span class="seconday">Our</span> Services
                    </h2>
                    <p>Some of the services that we offer </p>
                </div>
            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <a href="#" class="grid-parent">
                            <img src="/images/mobile_friendly.jpg" alt="" class="rsd">
                            <div class="absolute-box box2 front-frid-box transition init-block">
                                <i class="fa fa-mobile-phone head" aria-hidden="true"></i>
                                <h5>Mobile Friendly Website</h5>
                                <div class="post-blocked">
                                    <p>
                                        A mobile responsive website is no longer a luxury.
                                        It’s a necessity. With over 60% of all internet traffic
                                        now coming from a mobile device, it is essential that a website
                                        that can be viewed from any device; be it a smartphone, tablet
                                        or a desktop computer.
                                    </p>
                                    <div class="rebox rebox-services-box2">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="#" class="grid-parent">
                            <img src="/images/analytics.jpg" alt="" class="rsd">
                            <div class="absolute-box box5 front-frid-box transition init-block">
                                <i class="fa fa-line-chart head" aria-hidden="true"></i>
                                <h5>Analytics (Google Analytics)</h5>
                                <div class="post-blocked">
                                    <p>
                                        Collecting data is easier than ever.
                                        Analyzing it for insight is the new challenge marketers face.
                                        Google Analytics can provide a snapshot of the past and present,
                                        but insight into the future is where its true power lies.

                                        <!--Can you trust your data? Good analysis starts with accurate data
                                        collection, and our implementation specialists are trained to
                                        identify pitfalls in any GA setup - including inaccuracies and
                                        inefficiencies - and correct them. We’ll work with you to establish
                                        a measurement strategy, aligned with your business objectives.
                                        Then we’ll deploy advanced custom tracking, according to industry
                                        best practices, to set up a world class Google Analytics
                                        implementation you can trust.-->
                                    </p>
                                    <div class="rebox rebox-services-box5">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="#" class="grid-parent">
                            <img src="/images/content_dev.jpg" alt="" class="rsd">
                            <div class="absolute-box box7 front-frid-box transition init-block">
                                <i class="fa fa-clipboard head" aria-hidden="true"></i>
                                <h5>Content Development</h5>
                                <div class="post-blocked">
                                    <p>
                                        This is the most important parts of the website... "content".
                                        We provide this service such as site map development, content
                                        planning, witting, editing, keywords analysis.
                                    </p>
                                    <div class="rebox rebox-services-box7">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <a href="#" class="grid-parent">
                            <img src="/images/redesign.jpg" alt="" class="rsd">
                            <div class="absolute-box box6 front-frid-box transition init-block">
                                <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                <h5>Website Redesign</h5>
                                <div class="post-blocked">
                                    <p>
                                        "Refresh your online presence"
                                        Throughout the website redesign process, we don’t let go off “The Big Idea”.
                                        User expectations have changed over the past few years. Maybe the
                                        tapering traffic to your website is because your website is
                                        outdated in design and functionality.

                                        <!--Don’t worry. SPINX website redesign professionals are standing by
                                        to give your website an overhaul to improve your site’s performance.-->
                                    </p>
                                    <div class="rebox rebox-services-box6">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="#" class="grid-parent">
                            <img src="/images/hosting.jpg" alt="" class="rsd">
                            <div class="absolute-box box1 front-frid-box transition init-block">
                                <i class="fa fa-cloud head" aria-hidden="true"></i>
                                <h5>Web Hosting</h5>
                                <div class="post-blocked">
                                    <p>
                                        <!--We care about your website, even after it’s gone live. With
                                        secure web hosting and technical support included with every
                                        J-tech website, you can be sure that your website is available
                                        to your visitors anywhere, anytime.-->

                                        Website hosting is a critical factor for site speed.
                                        A slow website creates a poor user experience for website visitors
                                        and actually has a negative impact on your website’s SEO and
                                        rankings within the search engines. We offer state-of-the-art
                                        dedicated servers that allow for greater flexibility,
                                        speed and uptime for our customers at a cheaper price.
                                    </p>
                                    <div class="rebox rebox-services-box1">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="#" class="grid-parent">
                            <img src="/images/domain.jpg" alt="" class="rsd">
                            <div class="absolute-box box8 front-frid-box transition init-block">
                                <i class="fa fa-globe head" aria-hidden="true"></i>
                                <h5>Domain Name</h5>
                                <div class="post-blocked">
                                    <p>
                                        We give customers the choice of managing their own domain name and
                                        DNS records or handling it through our technical team.
                                    </p>
                                    <div class="rebox rebox-services-box8">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <a href="#" class="grid-parent">
                            <img src="/images/social.jpg" alt="" class="rsd">
                            <div class="absolute-box box4 front-frid-box transition init-block">
                                <i class="fa fa-share-alt head" aria-hidden="true"></i>
                                <h5>Social Media Integration</h5>
                                <div class="post-blocked">
                                    <p>
                                        Social Media has grown rapidly in popularity, the use of
                                        tools such us Twitter, Facebook, LinkedIn etc and therefore
                                        use of social media as a marketing strategy brings in a lot
                                        of impact and outreach for your business.
                                    </p>
                                    <div class="rebox rebox-services-box4">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="#" class="grid-parent">
                            <img src="/images/design.jpg" alt="" class="rsd">
                            <div class="absolute-box box9 front-frid-box transition init-block">
                                <i class="fa fa-lightbulb-o head" aria-hidden="true"></i>
                                <h5>Website Design</h5>
                                <div class="post-blocked">
                                    <p>
                                        "We make you look good online"... We combine great design with up
                                        to the minute online marketing trends to deliver websites that look
                                        good and actually work to grow your business. Our web design and
                                        web development processes have been tweaked, optimised and improved
                                        over the years of helping more than 4000 businesses get online.
                                        <!--So we can confidently say we can help you. We take an active
                                        approach in understanding you and your business, your industry
                                        and target market, as well as your competition to ensure that
                                        every design element works towards your business marketing goals.-->
                                    </p>
                                    <div class="rebox rebox-services-box9">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="#" class="grid-parent">
                            <img src="/images/maintenance.jpg" alt="" class="rsd">
                            <div class="absolute-box box3 front-frid-box transition init-block">
                                <i class="fa fa-gears head" aria-hidden="true"></i>
                                <h5>Website Maintenance</h5>
                                <div class="post-blocked">
                                    <p>
                                        We provide this service to all new and existing clients
                                        even if we didn't develop your website.
                                    </p>
                                    <div class="rebox rebox-services-box3">
                                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
