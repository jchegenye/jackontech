        <section class="tips-part" id="tips-part">
            <div class="section-seperator">
                <div class="container-template">
                    <h2>
                        <span class="seconday">Important</span> Tips
                    </h2>
                    <p>
                        “5 Aspects of Web Development.”
                    </p>
                </div>
            </div>
            <div class="content">
                <div class="content-template">
                    <ul>
                        <li>
                            <i class="icon-task-check"></i> Web Development is Not a “Once and Done” Project
                        </li>
                        <li>
                            <i class="icon-task-check"></i> Keep User Experience at the Heart of Web Development
                        </li>
                        <li>
                            <i class="icon-task-check"></i> Have a Management Plan
                        </li>
                        <li>
                            <i class="icon-task-check"></i> You Don’t Need to Start from Scratch
                        </li>
                        <li>
                            <i class="icon-task-check"></i> Ensure You Maintain Ownership
                        </li>
                        <li>
                            <i class="icon-task-check"></i> Gorgeous Visual Design
                        </li>
                        <li>
                            <i class="icon-task-check"></i> Logical User Experience
                        </li>
                        <li>
                            <i class="icon-task-check"></i> Mobile Friendly Design
                        </li>
                        <li>
                            <i class="icon-task-check"></i> Robust & Stable Structure
                        </li>
                        <li>
                            <i class="icon-task-check"></i> Plan Your Marketing Strategy
                        </li>
                        <li>
                            <i class="icon-task-check"></i> Your Web Design Company
                        </li>
                    </ul>
                </div>
            </div>
        </section>
