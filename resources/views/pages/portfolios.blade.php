        <section class="featured-part" id="Portfolios-part">
            <div class="section-seperator">
                <div class="container-template">
                    <h2>
                        <span class="seconday">Our</span> Portfolios
                    </h2>
                    <p>Some stuff we've build & launched.</p>
                </div>
            </div>

            <div class="portfolio-wrapper">
                <div class="col-md-12">
                    <ul class="filter">
                        <li><a class="active" href="#" data-filter="*">All</a></li>
                        <li><a href="#" data-filter=".law">Law</a></li>
                        <li><a href="#" data-filter=".church">Church</a></li>
                        <li><a href="#" data-filter=".tech">Technology</a></li>
                        <li><a href="#" data-filter=".orgs">Organisations</a></li>
                    </ul>
                </div>

                <div class="portfolio-items">
                    <div class="col-md-4 col-sm-6 work-grid law graphic">
                        <div class="portfolio-content">
                                <img class="img-responsive" src="images/works/kepsoc2.png" alt="">
                            <div class="portfolio-cover">
                                <a href="images/works/kepsoc.png">
                                    <i class="fa fa-camera-retro"></i>
                                </a>

                                {{--<a href="#" class="share-hover"><i class="fa fa-facebook"></i></a>--}}

                                <h5>KEPSOC</h5>
                                <p>Law/Criminology</p>
                                <div class="social-icons">
                                    <hr>
                                    Share this on
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-whatsapp"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-overlay">
                                <a href="images/works/kepsoc.png"><i class="fa fa-camera-retro"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 work-grid church bootstrap">
                        <div class="portfolio-content">
                            <img class="img-responsive" src="images/works/hrwm.png" alt="">
                            <div class="portfolio-cover">
                                <a href="images/works/kepsoc.png"><i class="fa fa-camera-retro"></i></a>
                                <h5>HRWM</h5>
                                <p>Church (Juja Town)</p>
                                <div class="social-icons">
                                <hr>
                                    Share this on
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-whatsapp"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-overlay">
                                <a href="images/works/hrwm.png"><i class="fa fa-camera-retro"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 work-grid tech bootstrap">
                        <div class="portfolio-content">
                            <img class="img-responsive" src="images/works/jtech.png" alt="">
                            <div class="portfolio-cover">
                                <a href="images/works/kepsoc.png"><i class="fa fa-camera-retro"></i></a>
                                <h5>J-tech Company</h5>
                                <p>Web Development</p>
                                <div class="social-icons">
                                <hr>
                                    Share this on
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-whatsapp"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-overlay">
                                <a href="images/works/jtech.png"><i class="fa fa-camera-retro"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 work-grid orgs bootstrap" style="z-index: inherit;">
                        <div class="portfolio-content">
                            <img class="img-responsive" src="images/works/makeithappen2.png" alt="">
                            <div class="portfolio-cover">
                                <a href="images/works/kepsoc.png"><i class="fa fa-camera-retro"></i></a>
                                <h5>Make It Happen</h5>
                                <p>Organisation</p>
                                <div class="social-icons">
                                <hr>
                                    Share this on
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-whatsapp"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-overlay">
                                <a href="images/works/makeithappen.png"><i class="fa fa-camera-retro"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6 work-grid orgs bootstrap" style="z-index: inherit;">
                        <div class="portfolio-content">
                            <img class="img-responsive" src="images/works/smartvisual.png" alt="">
                            <div class="portfolio-cover">
                                <a href="images/works/smartvisual.png"><i class="fa fa-camera-retro"></i></a>
                                <h5>Smart Visual</h5>
                                <p>Media Company</p>
                                <div class="social-icons">
                                    <hr>
                                    Share this on
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-whatsapp"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-overlay">
                                <a href="images/works/smartvisual.png"><i class="fa fa-camera-retro"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
