        <section class="contact-form" id="contact-form">
            <div class="section-seperator">
                <div class="container-template">
                    <h2>
                        <span class="seconday">Contact</span> Us Today
                    </h2>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="contact-text">
                            <p>
                                We are ready and waiting to help you make the most out of your business.
                                Call or email us today to schedule a meeting, and we’ll take care of the rest.
                            </p>
                            <br>
                            <i class="fa fa-map-marker"></i> <span>Nairobi - Kenya</span><br>
                            <i class="fa fa-phone"></i><span>(+254) 0711494289</span><br>
                            <i class="fa fa-envelope"></i><span>jtechinfo3@gmail.com</span><br>
                            <i class="fa fa-globe"></i><span>www.j-tech.tech</span>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="contact-text">
                            <div class="contact-form">
                                <form id="contact-us" method="post" action="{{url('save-contact')}}">
                                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                    <div class="col-xs-6 ">
                                        <div class="form-group">
                                            <input type="text" name="name" value="{{old('name')}}" id="name" class="form" placeholder="Name" />
                                            @if ($errors->has('name'))
                                                <span class="text text-danger">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="email" value="{{old('email')}}" id="email" class="form" placeholder="Email" />
                                            @if ($errors->has('email'))
                                                <span class="text text-danger">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="subject" value="{{old('subject')}}" id="subject" class="form" placeholder="subject" />
                                            @if ($errors->has('subject'))
                                                <span class="text text-danger">{{ $errors->first('subject') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <textarea name="bodyMessage" id="bodyMessage" class="form textarea"  placeholder="Message">{{old('bodyMessage')}}</textarea>
                                            @if ($errors->has('subject'))
                                                <span class="text text-danger">{{ $errors->first('subject') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="relative fullwidth col-xs-12">
                                        <button type="submit" id="submit" name="submit" class="form-btn semibold">Send Message</button>
                                    </div>
                                    <div class="clear"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
        </section>
