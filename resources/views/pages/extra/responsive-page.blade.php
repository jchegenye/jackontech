<!DOCTYPE html>
<html ng-app="ResponsiveTest">
<head>
    <meta charset="utf-8">
    <title>Test responsiveness of your website | J-Tech Company KE</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A Kenyan company that develop's powerful and responsive websites that respond to the users devices, platforms and screen size">
    <meta name="keywords" content="test responsive web design, responsive web design, responsive design testing, responsive web design testing, Responsive Web Design Tools">
    <meta name="author" content="Jackson C. Asumu">
    <meta name="copyright" content="J-Tech Company KE">

    <meta property="og:url" content="https://www.j-tech.tech" />

    <meta property="fb:app_id"        content="1000499559999755" />
    <meta property="fb:admins"        content="Jackson Asu Chegenye" />

    <meta property="og:url"           content="http://www.j-tech.tech" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="J-Tech Company KE | Responsive Website Tester" />
    <meta property="og:description"   content="A Kenyan company that develop's powerful and responsive websites that respond to the users devices, platforms and screen size." />
    <meta property="og:image"         content="http://www.j-tech.tech/images/responsive-design.jpg" />

    <link rel="icon" href="favicon.png" type="image/x-icon" />

    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <!-- <link href="assets/vendor/bootstrap/css/bootstrap-theme.css" rel="stylesheet" media="screen"> -->
    <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" media="screen">
    <link href="assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" media="screen">
    <link href="assets/dist/css/style.min.css" rel="stylesheet" media="screen">

    <!--[if lt IE 9]>
    <script src="assets/vendor/jquery/jquery-1.10.2.min.js"></script>
    <script src="assets/vendor/respond/respond.min.js"></script>
    <![endif]-->
    <!--[if gte IE 9]><!-->
    <script src="assets/vendor/jquery/jquery-2.0.3.min.js"></script>
    <!--<![endif]-->
    <script type="text/javascript" src="assets/vendor/jquery-ui/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script type="text/javascript" src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/vendor/angular/angular.min.js"></script>
    <script type="text/javascript" src="assets/dist/js/responsivetest.min.js"></script>

    <script src="assets/dist/js/scripts.js"></script>

    <!--[if lte IE 8]>
    <script type="text/javascript">
    document.createElement('rt-device');
    </script>
    <![endif]-->

    <!--########################### Google Analytics ##########################-->
    {!! Analytics::render() !!}
</head>

<body ng-controller="IndexController" ng-init="init()">

    <!-- nav: -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="assets/dist/img/favicon.png" alt="J-tech logo" /></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a data-toggle="modal" href="{{url('#request')}}">Request</a></li>
                    </ul>
                    <form class="navbar-form navbar-left rt-navbar">
                        <input type="text" class="form-control" ng-model="url" name="url" style="width:275px; margin-right: 20px;" placeholder="Enter the URL" rt-keyup="onKeyup" />
                        <input type="text" class="form-control" ng-model="w" name="w" style="width: 60px;" placeholder="w" disabled/>
                        <span style="color: #008080;">x</span>
                        <input type="text" class="form-control" ng-model="h" name="h" style="width: 60px;" placeholder="h" disabled/>
                        <button type="button" class="btn btn-success" ng-click="rotate()"><i class="fa fa-retweet"></i> Rotate</button>
                    </form>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown" ng-repeat="device in SUPPORTED_DEVICES" id="@{{ device.type }}">
                            <a href="#@{{ device.type }}" class="dropdown-toggle" data-toggle="dropdown" title="@{{ device.title }}"><i class="fa @{{ device.icon }} fa-2x"></i></a>
                            <rt-device brands="device.brands"></rt-device>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Help
                                <span class="fa fa-question"></span></a>
                            <ul class="dropdown-menu">
                                <li class="help">
                                    <p>1. Enter your website's URL e.g http://www.example.com & press enter.</p>
                                </li>
                                <li>
                                    <p>
                                        2. Select any device i.e tablet, phone, laptop or desktop.
                                    </p>
                                </li>
                                <li class="help">
                                    <p>3. Rotate the screen between landscape & portrait</p>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{url('/#contact-form')}}">Need More Help?</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

    <div class="rt-ruler"></div>

    <div class="rt-iframe" ng-style="{ width: w / pxd + 15 + 'px', height: h / pxd + 'px' }" rt-resizable ng-show="!loading" id="iframe">
        <iframe ng-src="@{{ frameSrc }}" rt-frame-loading></iframe>
    </div>

    <!-- about: -->
    <div class="modal fade" id="request">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h2 class="modal-title text-center">Get A Responsive Website</h2>
                </div>
                <div class="modal-body text-left">
                    <p style="font-size: 20px;">
                        After testing your website, what did you find out:<br>
                        <ul>
                            <li><em>was it responsive to the devices you tested with?</em></li>
                            <li><em>would you like us to make it more responsive for you?</em></li>
                        </ul>
                    </p>
                    <p>
                        <a href="{{url('/#contact-form')}}" class="btn btn-success">
                            Talk to us today
                        </a>
                        <a href="{{url('/')}}" class="btn btn-default " data-dismiss="modal" aria-hidden="true">
                            Go back
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- :about -->

    <footer class="text-center" id="footer">
        <div class="footer-below">
            <div class="container-fluid">
                Copyright &copy; J-tech Company KE {{date('Y')}}
            </div>
        </div>
    </footer>

</body>
</html>