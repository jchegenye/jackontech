<!DOCTYPE html>
<html lang="en-US">
<head>

    <!-- ************************* Meta Tags *********************** -->
    @include('layouts.meta-tags')

    <!-- ************************* Css links *********************** -->
    @include('layouts.css')

    <!--########################### Google Analytics ##########################-->
    {!! Analytics::render() !!}
</head>
<body>

        <!-- =============================  Forms  Messages ====================== -->
        @include('layouts.messages')

        <!-- =========================== 2. Menu ================================= -->
        @include('layouts.menu ')
        <!-- =========================== 3. Share Menu =========================== -->
        @include('layouts.share-menu')
        <!-- =============================  Pre-loader =========================== -->
        @include('layouts.preload')

        <!-- -------------------------- 4. Home -------------------------------- -->
        @include('pages.home')
        <!-- -------------------------- 5. About Us ---------------------------- -->
        @include('pages.about-us')
        <!-- -------------------------- 6. Our Services ------------------------ -->
        @include('pages.our-services')
        <!-- -------------------------- 7. Testimonial ------------------------- -->
        @include('pages.testimonial')
        <!-- -------------------------- 8. Portfolios -------------------------- -->
        @include('pages.portfolios')
        <!-- -------------------------- 9. Important Tips ---------------------- -->
        @include('pages.important-tips')
        <!-- -------------------------- 10. Pricing ---------------------------- -->
        @include('pages.pricing')
        <!-- -------------------------- 11. Get Our Services ------------------- -->
        @include('pages.get-our-services')
        <!-- -------------------------- 12. Team Members ----------------------- -->
        @include('pages.team-members')
        <!-- -------------------------- 13. Contact Us ------------------------- -->
        @include('pages.contact')

        <!-- -------------------------- 14. Footer ----------------------------- -->
        @include('layouts.footer')

        <!-- Holder for mobile navigation -->
        <div class="mobile-nav">
            <ul class="menu">
            </ul>
            <a href="#" class="close-link">
                <i class="fa fa-arrow-circle-up"></i>
            </a>
        </div>

    <!-- *************************** Scripts *************************** -->
    @include('layouts.scripts')

</body>
</html>