
@include('layouts.head')

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div style="margin-top:50px; margin-bottom:50px">
            <h1>Radio Africa Limited</h1>
                <h4>Instructions:</h4>
                    <ul>
                        <li>
                            Print the numbers 1..100
                            <h6>Solu:</h6>
                            <div class="alert alert-info">
                                @for( $i=1; $i<=100; $i++ ) 
                                    {{$i}}
                                @endfor
                            </div>
                            <hr>
                        </li>
                        <li>
                            For multiples of 3, print "Mpasho" instead of the number
                            <h6>Solu:</h6>
                            <div class="alert alert-info">
                                @for( $i=1; $i<=100; $i++ )
                                    @if($i%3 == 0 ) 
                                        <b>Mpasho</b>
                                    @endif
                                @endfor
                            </div>
                            <hr>
                        </li>
                        <li>
                            For multiples of 5, print "Star" instead of the number
                            <h6>Solu:</h6>
                            <div class="alert alert-info">
                                @for( $i=1; $i<=100; $i++ )
                                    @if($i%5 == 0) 
                                        <b>Star</b>
                                    @endif
                                @endfor
                            </div>
      
                            <hr>
                        </li>
                        <li>
                            For multiples of 3 and 5, print "MpashoStar" instead of the number
                            <h6>Solu:</h6>
                            <div class="alert alert-info">
                                @for( $i=1; $i<=100; $i++ )
                                    @if($i%3 == 0 && $i%5 == 0) 
                                        <b>MpashoStar</b>
                                    @endif
                                @endfor
                            </div>
           
                        </li>
                        <li>
                            Print it out as html/css so if we run it on a web server, it'll work.
                            <h6>
                                This Program is written in PHP using Laravel 5 Framework and Bootstrap - CSS/HTML</h6>
                        </li>
                    </ul>
            </div>
        </div>
         <div class="row">
            <div class="col-md-4">
                <div style="margin-top:50px; margin-bottom:50px">
                    <h5>Explanation</h5>
                    <hr>
                        <p>My thinking</p>

                        <p>
                        if (100 is divisible by 3) then
                                echo "Mpasho"
                        </p>
                        <p>
                            else if (100 is divisible by 5) then
                                echo "Star"
                        </p>
                        <p>
                            else if (100 is divisible by 3 and 5) then
                                echo "MpashoStar"
                        </p>
                        <p>
                        end if
                        </p>
                    <h5>Snipet:</h5>
                    <hr>
                        <ul>
                        <li>
                                for( $i=1; $i<=100; $i++ ) 
                                    echo $i;
                                endfor
                            <hr>
                        </li>
                        <li>
                                for( $i=1; $i<=100; $i++ )
                                    if($i%3 == 0 ) 
                                        echo "<b>Mpasho</b>";
                                    endif
                                endfor
                            <hr>
                        </li>
                        <li>
                                for( $i=1; $i<=100; $i++ )
                                    if($i%5 == 0) 
                                        echo "<b>Star</b>";
                                    endif
                                endfor
                            <hr>
                        </li>
                        <li>
                                for( $i=1; $i<=100; $i++ )
                                    if($i%3 == 0 && $i%5 == 0) 
                                        echo "<b>MpashoStar</b>";
                                    endif
                                endfor
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- JS -->
@include('layouts.scripts')

<!DOCTYPE html>
<html lang="en">
<head>
    <title>J-Tech Company KE | Enquiry / Support</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <meta property="og:url" content="https://www.j-tech.te" />

    <meta property="fb:app_id"        content="1000499559999755" />
    <meta property="fb:admins"        content="Jackson Asu Chegenye" />

    <meta property="og:url"           content="http://www.j-tech.tech" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="J-Tech Company KE | Responsive Website Tester" />
    <meta property="og:description"   content="A Kenyan company that develop's powerful and responsive websites that respond to the users devices, platforms and screen size." />
    <meta property="og:image"         content="http://www.jtech.dev/images/responsive-design.jpg" />


    <style type="text/css">
        body, table{
            font-family:cursive;
        }
        .facebook{
            background-color: #0179FF;
            color: #FFF;
            padding: 5px 15px;
            text-decoration: none;
        }
        .twitter{
            background-color: #01C5FF;
            color: #FFF;
            padding: 5px 15px;
            text-decoration: none;
            margin: 0px 15px;
        }
        .google-plus{
            background-color: #FF0101;
            color: #FFF;
            padding: 5px 15px;
            text-decoration: none;
        }
        .appleFooter a {
            color: #999999; font-size:12px;
            text-decoration: none;
        }
    </style>

    {{--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js" type="text/javascript"></script>--}}

</head>
<body style="margin: 0; padding: 0; background:rgb(245, 245, 245);">

{{--<script>
    document.getElementById('shareBtn').onclick = function() {
        FB.ui({
            display: 'popup',
            method: 'share',
            href: 'https://developers.facebook.com/docs/',
        }, function(response){});
    }
</script>--}}

{{--<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1000499559999755',
      xfbml      : true,
      version    : 'v2.6',
        method: 'share',
        href: 'https://developers.facebook.com/docs/',
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>--}}

<table border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
        <td style="
                padding: 5px 15px;
                background: #008080;
                border-bottom: 5px solid #F5F5F5;">
            <a href="http://www.j-tech.tech/">
                <img src="http://www.jtech.dev/logo.png" style="width: 55px;">
            </a>
        </td>
    </tr>
    <tr>
        <td bgcolor="color: #B0BEC5;" align="center" style="padding:20px 20px; background-color: #fff;" class="section-padding">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left"
                                                style="color: #675C5C; padding-bottom: 0.2em; text-transform:capitalize;"
                                                class="padding-copy">Dear { { $ user->name } },
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="color: #675C5C; ">
                                                <p>
                                                    Thank you for contacting us. We've received
                                                    your email and on of our team member's will
                                                    reach out to you.
                                                </p>
                                                <p style="
                                                    background-color: rgba(245, 245, 245, 0.59);
                                                    border-top: 1px solid;
                                                    margin: 15px -20px;
                                                    padding: 15px 20px;
                                                    color:#FF4500;">
                                                    Take few minutes of your time to share your
                                                    love for us on social accounts. Click the links
                                                    below.
                                                    <br><br>
                                                    {{--They should be able to share j-tech content to
                                                    there social accounts. use this link
                                                    http://www.j-tech.tech/#about-j-tech i.e comment--}}
                                                    <a href="#" class="facebook">
                                                        Facebook
                                                    </a>
                                                    <a href="#" class="twitter">
                                                        Twitter
                                                    </a>
                                                    <a href="#" class="google-plus">
                                                        Google
                                                    </a>
                                                    {{--<div class="fb-share-button" data-href="http://www.j-tech.tech/test-responsive-website#u=http://smartvisual.j-tech.tech|750|1334|2" data-layout="button_count" data-mobile-iframe="true"></div>--}}

                                                    <button id="shareBtn" class="btn btn-success clearfix">Share Dialog</button>
                                                    {{-- Facebook Script --}}


                                                    <script>
                                                        window.fbAsyncInit = function() {
                                                            FB.init({
                                                                appId      : '1000499559999755',
                                                                xfbml      : true,
                                                                version    : 'v2.6',
                                                            });
                                                        };

                                                        (function(d, s, id){
                                                            var js, fjs = d.getElementsByTagName(s)[0];
                                                            if (d.getElementById(id)) {return;}
                                                            js = d.createElement(s); js.id = id;
                                                            js.src = "//connect.facebook.net/en_US/sdk.js";
                                                            fjs.parentNode.insertBefore(js, fjs);
                                                        }(document, 'script', 'facebook-jssdk'));

                                                        document.getElementById('shareBtn').onclick = function() {
                                                            FB.ui({
                                                                display: 'popup',
                                                                method: 'share',
                                                                href: 'https://www.j-tech.tech',
                                                            }, function(response){});
                                                        }
                                                    </script>


                                                    {{--<script type="text/javascript">
                                                        $(document).ready(function(){
                                                        $('#share_button').click(function(e){
                                                        e.preventDefault();
                                                        FB.ui(
                                                        {
                                                        method: 'feed',
                                                        name: 'J-Tech Company KE',
                                                        link: 'http://www.j-tech.tech',
                                                        picture: 'http://www.j-tech.tech/images/mobile_friendly.jpg',
                                                        caption: 'Responsive website',
                                                        description: 'Test your website just to see if its Mobile Friendly ',
                                                        message: 'http://www.j-tech.tech/test-responsive-website#u=http://smartvisual.j-tech.tech|750|1334|2'
                                                        });
                                                        });
                                                        });
                                                    </script>

                                                    <img src = "share_button.png" id = "share_button">--}}
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"
                                                style="color: #675C5C;"
                                                class="padding-copy">Many Thanks,
                                                <br>
                                                J-Tech Team
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" align="center" style="
    border-top: 5px solid #F9F9F9;
    background-color: #FFF;
    border-bottom: 2px solid #F5F5F5;">
    <tr>
        <td align="center" style="padding:0px 20px;">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                <tr>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                                <td align="left"
                                    style="padding: 20px 0 0 0; font-size: 12px; color: #666666;"
                                    class="padding-copy">
                                    <span class="appleFooter" style="color:#666666;">
                                    <a href="www.j-tech.tech">Powered By, J-Tech Company KE.</a>
                                        {{date('Y') }}</span><br><span class="original-only"
                                                                       style=" color: #444444;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>