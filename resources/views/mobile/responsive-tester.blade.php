<!DOCTYPE html>
<html lang="en-US">
<head>

    <!-- ************************* Meta Tags *********************** -->
    @include('layouts.meta-tags')

            <!-- ************************* Css links *********************** -->
    @include('layouts.css')

</head>
<body>

    <!--########################### Google Analytics ##########################-->
    {!! Analytics::render() !!}

    <!-- =============================  Forms  Messages ====================== -->
    @include('layouts.messages')

    <!-- =========================== 2. Menu ================================= -->
    @include('layouts.menu ')
    <!-- =========================== 3. Share Menu =========================== -->
    @include('layouts.share-menu')
    <!-- =============================  Pre-loader =========================== -->
    @include('layouts.preload')

    <section class="mobile-agent" id="mobile-agent">
        <div class="container">
            <div class="col-sm-12">

                <div class="content">
                    <div class="mobile-body">
                        <h3>
                            <i class="fa fa-wrench size-28" aria-required="true"></i><br>
                            Responsive website tester
                        </h3>
                        <p>
                            Almost there, but unfortunate you are using a mobile device which we
                            doesn't support.
                        </p>
                        <p>
                            <b>NOTE:</b> We recommend that you use your desktop or tablet
                            device to carry out your website test.
                        </p>
                        <hr>
                        <p>
                            If you experience any problem, kindly let us know.<br>
                            <i class="fa fa-phone"></i> Call/SMS us on 0711494289 or<br>
                            <i class="fa fa-envelope"></i> Email us
                            <a href="{{url('/#contact-form')}}">click here</a>
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer class="text-center">
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; J-tech Company KE {{date('Y')}}
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Holder for mobile navigation -->
    <div class="mobile-nav">
        <ul class="menu">
        </ul>
        <a href="#" class="close-link">
            <i class="fa fa-arrow-circle-up"></i>
        </a>
    </div>

<!-- *************************** Scripts *************************** -->
@include('layouts.scripts')

</body>
</html>