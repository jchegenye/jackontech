<!DOCTYPE html>
<html lang="en">

<head>

    <link rel="canonical" href="http://j-tech.tech/" />

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf_token" content="{{ csrf_token() }}">

    <title class="title">J-tech | Home</title>

    <meta class="description" content="A Kenyan firm that builds powerfull and responsive websites that respond to the users devices, platforms and screen size. You can see the same desktop version layout in Mobile, Tablet & iPad">
    <meta name="keywords" content="kenya, professional, society, criminology, organisation, crime ">
    <meta class="image" content="<img src='/logo.png'>"/>
    <meta name="copyright" content="j-tech is a registered trade mark Co. based in Kenya">
    <meta name="author" content="Chegenye Asumu Jackson">
    <meta name="reply-to" content="chegenyejackson@gmail.com">
    <meta name="web_author" content="http://j-tech.tech/">
    <meta name="abstract" content="J-tech Co.">

    <meta name="identifier-URL"CONTENT="http://j-tech.tech/">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('http://j-tech.tech/favicon.png') }}">
    
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    
    <!-- CSS start here -->
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.css" />

    <style>
        html, body {
            height: 100%;
        }
        body {
            background-image: url(http://j-tech.tech/../images/01.jpg);
            margin: 0;
            padding: 0;
            width: 100%;
            color: #fff;
            display: table;
            font-weight: 700;
            font-family: 'Lato';
            display: flex;
            position: relative;
            justify-content: center;
            flex-direction: column;
            background-repeat: no-repeat;
            background-size: cover;
            background-attachment: fixed;

        }
        .title{
           /* padding-top: 25%;*/
            text-align: -webkit-center;
        }
        .body-2{
            border-left: 1px solid #ddd;
            padding-left: 30px;
            letter-spacing: 0.1em;
            padding-top:5%;
            padding-bottom:5%;
        }
        .footer{
            margin-top:4%;
        }
        .btn-notify{
            background-color:transparent;
            color: #3469AB;
            border-radius: inherit;
            border: 1px solid #3469AB;
            font-weight:700;
        }
        .btn-notify:hover{
            background-color: #3469AB;
            color: #fff;
        }
        .fa-social{
            letter-spacing: 0.2em;
        }
        .contact-form{
            padding-top:20px;
        }
        .contact-form input[type="email"]{
            box-shadow: 0px 0px 6px transparent;
            background-color:transparent;
            border: 1px solid #3469AB;
            color:#3469AB;
        }
        .copyright{
            padding-top:15px;
        }
        .home-overlay{
            background-color:#000;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            opacity: 0.8;
        }
    </style>

</head>

    <body>
    <div class="home-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="title ">
                        <h1>
                            <image src="single-logo.png" style="width:70px;"></image> J-tech Co.
                        </h1>
                        <p>
                            <i><div class="element" style="letter-spacing: 0.4em;"></div></i>
                        </p>
                    </div>
                </div>

                <div class="col-sm-8">
                    <div class="body-2 ">
                        <p>
                            Hey Guys!
                        </p>
                        <p>
                            Welcome to J-tech Co.
                        </p>
                        <p>
                            Our website is coming soon...
                        </p>
                        <h4 >
                            In the mean time use the information below to connect with us:
                        </h4>

                        <div style="font-size: initial;">
                            <div class="address">
                                <p class="address-content " style="color:#3469AB;"><i class="fa fa-envelope-o">Email:</i>
                                    <a href="mailto:chegenyejackson@gmail.com">chegenyejackson@gmail.com</a>
                                </p>
                            </div>
                            <div class="address" style="border-bottom: 1px solid #3469AB; padding-bottom:5px;">
                                <p class="address-title " style="color:#3469AB;"><i class="fa fa-whatsapp">WhatsApp Me:
                                    </i> <a href="intent://send/+254711494289#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end">+254 711 494 289</a>
                                    {{--<a href="whatsapp://send?text=http://www.hivisasa.com/kiambu/news/118817">Share on whatsapp</a>--}}
                                </p>
                            </div>
                        </div>

                        <form action="{{'/subscribe'}}" class="input-group input-group-lg contact-form" id="subscribeForm" method="POST">
                            <span class="input-group-addon" style="background-color:transparent; color:#3469AB;  border: 1px solid #3469AB;">
                                <i class="fa fa-envelope"></i>
                            </span>
                                <input type="hidden" value="{{ csrf_token() }}" name="_token">
                                <input type="email" class="form-control" data-validation="email" id="sEmail" name="sEmail" placeholder="Enter your email address">
                            <span class="input-group-btn">
                                <button class="btn btn-notify" type="submit">Get Notified!</button>
                            </span>
                        </form>

                        <div class="copyright">
                            <div class="fa-social">Share us with:
                                <!-- <span>Follow us: &nbsp</span> -->
                                <a href="#"><i class="fa fa-facebook"></i>acebook</a> &nbsp
                                <a href="#"><i class="fa fa-twitter"></i>twitter</a> &nbsp
                                <a href="#"><i class="fa fa-google-plus"></i>google+</a> &nbsp
                                <a href="whatsapp://send?text=Do you need a website? call:0711494289 OR visit: http://j-tech.tech/"><i class="fa fa-whatsapp"></i>WhatsApp</a>
                            </div>
                            &copy; {{date('Y')}}
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </body>

</html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript" src="js/typed.js"></script>
<script type="text/javascript" src="js/main.js"></script>




