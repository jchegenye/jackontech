<!DOCTYPE html>
<html lang="en">
<head>
    <title>J-Tech Company KE | Enquiry / Support</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <style type="text/css">
        body, table{
            font-family:cursive;
        }
        .appleFooter a {
            color: #999999; font-size:12px;
            text-decoration: none;
        }
    </style>
</head>
<body style="margin: 0; padding: 0; background:rgb(245, 245, 245);">

<table border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
        <td style="
                padding: 5px 15px;
                background: #008080;
                border-bottom: 5px solid #F5F5F5;">
            <a href="http://www.j-tech.tech/">
                <img src="http://www.j-tech.tech/logo.png" style="width: 55px;">
            </a>
        </td>
    </tr>
    <tr>
        <td bgcolor="color: #B0BEC5;" align="center" style="padding:20px 20px; background-color: #fff;" class="section-padding">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left"
                                                style="color: #675C5C; padding-bottom: 0.2em; text-transform:capitalize;"
                                                class="padding-copy">Dear J-tech,
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="color: #675C5C; ">
                                                <p>
                                                    {{ $name }}
                                                    <br>
                                                    {{ $subject }}
                                                    <br>
                                                    {{ $bodyMessage }}
                                                    <br>
                                                    {{ $email }}
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"
                                                style="color: #675C5C;"
                                                class="padding-copy">Many Thanks,
                                                <br>
                                                {{ $name }}
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" align="center" style="
    border-top: 5px solid #F9F9F9;
    background-color: #FFF;
    border-bottom: 2px solid #F5F5F5;">
    <tr>
        <td align="center" style="padding:0px 20px;">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                <tr>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0" align="center">
                            <tr>
                                <td align="left"
                                    style="padding: 20px 0 0 0; font-size: 12px; color: #666666;"
                                    class="padding-copy">
                                    <span class="appleFooter" style="color:#666666;">
                                    <a href="www.j-tech.tech">Powered By, J-Tech Company KE.</a>
                                        {{date('Y') }}</span><br><span class="original-only"
                                                                       style=" color: #444444;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

</body>
</html>