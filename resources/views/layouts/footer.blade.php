        <footer class="text-center">
            <div class="footer-above">
                <div class="container">
                    <div class="row">
                        <div class="footer-col col-md-4">
                            <h3>Get Our News Letters</h3>

                            <form action="/subscribe" class="input-group input-group-lg" id="subscribeForm" method="POST">
                                    <span class="input-group-addon get-in-touch-letter-icon">
                                        <i class="fa fa-envelope"></i>
                                    </span>

                                <input type="email" class="form-control" data-validation="email" id="sEmail" name="sEmail" placeholder="Enter your email address">

                                    <span class="input-group-btn">
                                        <button class="btn btn-info" type="submit">Go</button>
                                    </span>
                            </form>

                        </div>
                        <div class="footer-col col-md-4">
                            <h3>Share your love for us</h3>
                            <ul class="list-inline">
                                <li>
                                    <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                                </li>
                                <li>
                                    <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                                </li>
                                <li>
                                    <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-dribbble"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer-col col-md-4">
                            <h3>About J-tech</h3>
                            <p>
                                J-tech is Kenyan company that develop's powerful and responsive
                                websites that respond to the users devices, platforms and screen size.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-below">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            Copyright &copy; J-tech Company KE {{date('Y')}}
                        </div>
                    </div>
                </div>
            </div>
        </footer>