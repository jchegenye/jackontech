    <link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="css/animate.min.css"/>
    <link rel="stylesheet" href="css/picto-foundry-emotions.css" />
    <link rel="stylesheet" href="css/picto-foundry-household.css" />
    <link rel="stylesheet" href="css/picto-foundry-shopping-finance.css" />
    <link rel="stylesheet" href="css/picto-foundry-general.css" />
    <link href="css/owl.carousel.css" rel="stylesheet"/>
    <link href="css/magnific-popup.css" rel="stylesheet"/>
    <link href="css/font-awesome.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/responsive.css" />
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Favicon -->
    <link rel="icon" href="favicon.png" type="image/x-icon" />