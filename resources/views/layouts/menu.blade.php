<nav class="navbar">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand newHeaderLogo" href="{{url('/')}}">
                <img src="logo.png" data-active-url="logo.png" alt="">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right main-nav menu">
                {{--@if (Agent::isDesktop())
                    <a href="{{url('/#about-j-tech')}}" class="smoothScroll" title="What Is
                    J-tech?">
                        <i class="step icon-information size-24"></i>
                        About
                    </a>
                @else
                    <a href="{{url('#about-j-tech')}}" class="smoothScroll" title="What Is J-tech?">
                        <i class="step icon-information size-24"></i>
                        About
                    </a>
                @endif--}}
                <li class="hidden">
                    <a href="{{url('#page-top')}}"></a>
                </li>
                <li>
                    <a href="{{url('#about-j-tech')}}" class="smoothScroll" title="What Is J-tech?">
                        <i class="step icon-information size-24"></i>
                        About
                    </a>
                </li>
                <li>
                    <a href="{{url('#stastistical-part')}}" class="smoothScroll" title="Services">
                        <i class="icon-reciept-1 size-24"></i>
                        Services
                    </a>
                </li>
                <li>
                    <a href="{{url('#Portfolios-part')}}" class="smoothScroll" title="Portfolios">
                        <i class="step icon-bulleted-list size-24"></i>
                        Portfolios
                    </a>
                </li>
                <li>
                    <a href="{{url('#pricing-part')}}" class="smoothScroll" title="Pricing">
                        <i class="step icon-cogs size-24"></i>
                        Pricing
                    </a>
                </li>
                <li>
                    <a href="{{url('#testimonials-part')}}" class="smoothScroll" title="Testimonial">
                        <i class="step icon-thumbs-up size-24"></i>
                        Testimonial
                    </a>
                </li>
                <li>
                    <a href="{{url('#contact-form')}}" onclick="{!! Analytics::trackEvent('Contact Us', 'pageview') !!}" class="smoothScroll" title="Contact Form">
                        <i class="step icon-envelope-3 size-28"></i>
                        Contact
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>