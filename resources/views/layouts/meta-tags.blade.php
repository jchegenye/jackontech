    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf_token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title> J-Tech Company KE | Responsive Website Development Company</title>

    <meta name="description" content="A Kenyan company that develop's powerful and responsive websites that respond to the users devices, platforms and screen size."/>
    <meta name="keywords" content="website developer kenya, website developer nairobi, responsive website developer kenya, jtech kenya">
    <meta name="copyright" content="J-tech Company KE">
    <meta name="author" content="Jackson C. Asumu">
    <meta name="reply-to" content="jtechinfo3@gmail.com">
    <meta name="identifier-URL" content="https://www.j-tech.tech/">
