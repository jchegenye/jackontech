@if(Session::has('internet_connection'))
    <div class="alert alert-info info">
        <a href="{{url('/#')}}"><i class="fa fa-xing"></i></a>
        <img src="{{asset('favicon.png')}}" class="img-responsive">
        {{ Session::get('internet_connection') }}
    </div>
@elseif(Session::has('successful_contact'))
    <div class="alert alert-success success">
        <a href="{{url('/#')}}"><i class="fa fa-xing"></i></a>
        <img src="{{asset('favicon.png')}}" class="img-responsive">
        {{ Session::get('successful_contact') }}
    </div>
@elseif(Session::has('successful_quote'))
    <div class="alert alert-success success">
        <a href="{{url('/#')}}"><i class="fa fa-xing"></i></a>
        <img src="{{asset('favicon.png')}}" class="img-responsive">
        {{ Session::get('successful_quote') }}
    </div>
@endif