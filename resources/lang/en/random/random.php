<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Random Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used when any form is sent.
    |
    */

    'company' => 'J-Tech Company KE',
    'internet_connection' => 'No internet connection ! This form could not be delivered.',

    /* == quote == */
    'successful_quote' => 'Thank You ! kindly check your email.',

    /* == contact us == */
    'successful_contact' => 'Thank You ! kindly check your email.',

];
