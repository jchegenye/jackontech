<?php namespace JackOnTech\JTech\ReusableCodes {

    /**
     * @author Jackson Asumu Chegenye
     *         0711494289
     *         chegenyejackson@gmail.com
     * @version 0.0.1
     * @copyright 2015-2016 j-tech.tech
     *
     * @File Handles Subscriptions
     */

    class GenerateVerificationCode{
        /**
         * Returns an new verify code. For every subscriber
         *
         * @return string
         */
        public static function generateVerifyCode($code)
        {
            $code =  "jtech" . str_random(42) . date('M,Y');
            return $code;
        }
    }
}
