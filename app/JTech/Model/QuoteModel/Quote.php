<?php namespace JackOnTech\JTech\Model\QuoteModel;

use Jenssegers\Mongodb\Model as Eloquent;

class Quote extends Eloquent
{

    /**
     * The database collection used by the model.
     *
     * @var string
     */
    protected $collection = "quote_request";

}
