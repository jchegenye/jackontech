<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*| - Pages URL's' - |*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('test-responsive-website', [
    'uses' => 'ResponsiveTesterController@getTester'
]);
Route::get('phptest', function () {
    return view('test');
});
Route::get('test', function () {
    return view('test2');
});


/*
|--------------------------------------------------------------------------
| Contact Us Routes
|--------------------------------------------------------------------------
*/
Route::post('save-contact', [
    'uses' => 'ContactUsController@emailContactForm'
]);

/*
|--------------------------------------------------------------------------
| Quote Routes
|--------------------------------------------------------------------------
*/
Route::post('save-quote', [
    'uses' => 'QuoteController@saveQuote'
]);

/*
|--------------------------------------------------------------------------
| Subscribe Routes
|--------------------------------------------------------------------------
*/
Route::post('save-email', [
    'uses' => 'SubscribeController@storeEmails'
]);
Route::get('/account/verify/{verify_code}', [
    'uses' => 'SubscribeController@verifyAccount'
]);