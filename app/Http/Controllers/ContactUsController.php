<?php namespace JackOnTech\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Exception;
/**
 * @author Jackson Asumu Chegenye <chegenyejackson@gmail.com> <0711494289>
 * @version 0.0.1
 * @copyright J-Tech Company KE
 *
 * @File Handles Contact Us Form
 */

class ContactUsController extends Controller {

    /**
     * Send contact details.
     *
     * @return
     */

    public function emailContactForm(){

        $rules = array (
            'name' => 'required|min:5',
            'email'  => 'required|email',
            'subject' => 'required|min:5|max:150',
            'bodyMessage' => 'required|min:5',
        );

        $validator  = Validator::make (Input::all(), $rules);

            if ($validator -> passes()){

                try {

                    $email_data = array(
                        'name' => Input::get('name'),
                        'email' => Input::get('email'),
                        'subject' => Input::get('subject'),
                        'bodyMessage' => Input::get('bodyMessage'),
                        '_token' => Input::get('_token'),
                    );

                    Mail::send('emails.contact.support-sender', $email_data, function($message)
                    {
                        $message->from(getenv('MAIL_FROM'), getenv('MAIL_NAME'));
                        $message->to(Input::get('email'), Input::get('name'));
                        $message->subject('Enquiry / Support | ' . Input::get('subject'));
                    });

                    Mail::send('emails.contact.support-receiver', $email_data, function($message)
                    {
                        $message->from( Input::get('email'), Input::get('name'));
                        $message->to(getenv('MAIL_FROM'), getenv('MAIL_NAME'));
                        $message->subject('Enquiry / Support | ' . Input::get('subject'));
                    });


                } catch (Exception $e) {

                    if ($e instanceof \Swift_SwiftException) {

                        Session::flash('internet_connection', 'No internet connection ! This form could not be delivered.');
                        return Redirect::to('/#contact-form');
                    }

                }

                {
                    Session::flash('successful_contact', 'Thank You ! kindly check your email.');
                    return Redirect::to('/#contact-form');
                }
            }
        else{
            return Redirect::to('/#contact-form')
                ->withInput()
                ->withErrors($validator->messages());
        }
    }
}