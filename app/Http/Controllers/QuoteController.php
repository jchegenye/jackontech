<?php namespace JackOnTech\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

use \JackOnTech\JTech\Model\QuoteModel\Quote;
use JackOnTech\JTech\ReusableCodes\DateFormats;
use Exception;

/**
 * @author Jackson Asumu Chegenye <chegenyejackson@gmail.com> <0711494289>
 * @version 0.0.1
 * @copyright J-Tech Company KE
 *
 * @File Handles Contact Us Form
 */

class QuoteController extends Controller {

    public function saveQuote(){

        $rules = array(
            'full_names' => 'required|min:5',
            'phone_number' => 'numeric|min:10',
            'your_email' => 'required|same:your_email',
            'project_type' => 'required|min:5',
            'project_description' => 'required|min:5|max:500',
            'county' => 'required',
            'hear_about_us' => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator -> passes()) {

            try {

                $quote_data = array(
                    'full_names' => ucwords(str_replace("_", " ", Input::get('full_names'))),
                    'company_name' => Input::get('company_name'),
                    'phone_number' => Input::get('phone_number'),
                    'your_email' => Input::get('your_email'),
                    'project_type' => Input::get('project_type'),
                    'county' => Input::get('county'),
                    'hear_about_us' => Input::get('hear_about_us'),
                    'project_description' => Input::get('project_description'),
                    '_token' => Input::get('_token'),
                );

                Mail::send('emails.quote.quote_received', $quote_data, function($message)
                {
                    $message->from(getenv('MAIL_FROM'), getenv('MAIL_NAME'));
                    $message->to(Input::get('your_email'), Input::get('full_names'));
                    $message->cc(getenv('MAIL_FROM'), 'Software Engineer');
                    $message->subject('Quote Received | ' . getenv('MAIL_NAME'));
                });

            } catch (Exception $e) {

                if ($e instanceof \Swift_SwiftException) {

                    Session::flash('internet_connection', 'No internet connection ! This form could not be delivered.');
                    return Redirect::to('/#collapseQuote');
                }

            }
            {
                $date = new DateFormats();
                $Date1 = $date->date();

                $quote = new Quote;

                $quote_qid = Quote::orderBy('qid', 'DESC')->take(1)->get();
                if ($quote_qid->isEmpty()) {
                    $quote->qid = 1;
                } else {
                    foreach ($quote_qid as $count) {
                        $quote->qid = $count->qid + 1;
                    }
                }
                $quote->full_names = ucwords(str_replace("_", " ", Input::get('full_names')));
                $quote->company_name = Input::get('company_name');
                $quote->phone_number = Input::get('phone_number');
                $quote->your_email = Input::get('your_email');
                $quote->project_type = Input::get('project_type');
                $quote->county = Input::get('county');
                $quote->hear_about_us = Input::get('hear_about_us');
                $quote->project_description = Input::get('project_description');
                $quote->requested_date = $Date1['Date1'];
                $quote->save();

                Session::flash('successful_quote', 'Thank You ! kindly check your email.');
                return Redirect::to('/#collapseQuote');
            }

        } else {

            return Redirect::to('/#collapseQuote')->withInput()->withErrors($validator->messages());

        }

    }

}