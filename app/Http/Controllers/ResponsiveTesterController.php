<?php namespace JackOnTech\Http\Controllers;
use Jenssegers\Agent\Facades\Agent;


/**
 * @author Jackson Asumu Chegenye <chegenyejackson@gmail.com> <0711494289>
 * @version 0.0.1
 * @copyright J-Tech Company KE
 *
 * @File Handles Responsive Tester
 */
class ResponsiveTesterController extends Controller {

    public function getTester(){

        if (Agent::isDesktop()) {
            return view('pages.extra.responsive-page');
        }elseif(Agent::isTablet()){
            return view('mobile.responsive-tester');
        }else if(Agent::isMobile()) {
            return view('mobile.responsive-tester');
        }

    }

}